package EShop.lab2

import akka.actor.Cancellable

object Helper {
  def withCancel[T](timer: Cancellable)(f: => T): T = {
    timer.cancel()
    f
  }
}
