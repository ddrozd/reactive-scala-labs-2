package EShop.lab2

import EShop.lab2.CartActor.CloseCheckout
import EShop.lab2.Checkout._
import EShop.lab3.Payment
import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.event.{Logging, LoggingReceive}

import scala.concurrent.duration._
import scala.language.postfixOps

object Checkout {

  sealed trait Data
  case object Uninitialized                               extends Data
  case class SelectingDeliveryStarted(timer: Cancellable) extends Data
  case class ProcessingPaymentStarted(timer: Cancellable) extends Data

  sealed trait Command
  case object StartCheckout                       extends Command
  case class SelectDeliveryMethod(method: String) extends Command
  case object CancelCheckout                      extends Command
  case object ExpireCheckout                      extends Command
  case class SelectPayment(payment: String)       extends Command
  case object ExpirePayment                       extends Command
  case object ReceivePayment                      extends Command
  case object Expire                              extends Command

  sealed trait Event
  case object CheckOutClosed                        extends Event
  case class PaymentStarted(payment: ActorRef)      extends Event
  case object CheckoutStarted                       extends Event
  case object CheckoutCancelled                     extends Event
  case class DeliveryMethodSelected(method: String) extends Event

  def props(cart: ActorRef) = Props(new Checkout(cart))
}

class Checkout(
  cartActor: ActorRef
) extends Actor {

  import Helper.withCancel

  private val scheduler = context.system.scheduler
  private val log = Logging(context.system, this)

  val checkoutTimerDuration = 1 seconds
  val paymentTimerDuration = 1 seconds

  def receive: Receive = LoggingReceive {
    case StartCheckout =>
      val timer = schedule(checkoutTimerDuration, ExpireCheckout)
      context.become(selectingDelivery(timer))
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case SelectDeliveryMethod(_) => context.become(selectingPaymentMethod(timer))
    case CancelCheckout | ExpireCheckout => context.become(cancelled)
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case SelectPayment(method) => withCancel(timer) {
        val paymentTimer    = schedule(paymentTimerDuration, ExpirePayment)
        val paymentActorRef = context.actorOf(Payment.props(method, sender, self), "payment")
        sender() ! PaymentStarted(paymentActorRef)
        context.become(processingPayment(paymentTimer))
      }
    case CancelCheckout | ExpireCheckout => context.become(cancelled)
  }

  def processingPayment(timer: Cancellable): Receive = {
    case ReceivePayment => withCancel(timer) {
        cartActor ! CloseCheckout
        context.become(closed)
      }
    case CancelCheckout | ExpirePayment | ExpireCheckout => context.become(cancelled)
  }

  def cancelled: Receive = {
    case event => log.error("Checkout cancelled.", event)
  }

  def closed: Receive = {
    case event => log.error("Checkout closed.", event)
  }

  private def schedule(time: FiniteDuration, message: Command): Cancellable =
    scheduler.scheduleOnce(delay = time, receiver = self, message = ExpireCheckout)(
      context.system.dispatcher
    )
}
