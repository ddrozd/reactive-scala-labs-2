package EShop.lab4

import EShop.lab2.{CartActor, Helper}
import EShop.lab3.Payment
import akka.actor.{ActorRef, Cancellable, Props}
import akka.event.{Logging, LoggingReceive}
import akka.persistence.{PersistentActor, SnapshotOffer}

import scala.util.Random
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object PersistentCheckout {

  def props(cartActor: ActorRef, persistenceId: String) =
    Props(new PersistentCheckout(cartActor, persistenceId))
}

class PersistentCheckout(
  cartActor: ActorRef,
  val persistenceId: String
) extends PersistentActor {

  import EShop.lab2.Checkout._
  private val scheduler = context.system.scheduler
  private val log       = Logging(context.system, this)
  val timerDuration     = 1.seconds

  private def updateState(event: Event, maybeTimer: Option[Cancellable] = None): Unit = {
    val timer = maybeTimer.getOrElse(schedule)
    event match {
      case CheckoutStarted                => context.become(selectingDelivery(timer))
      case DeliveryMethodSelected(method) => context.become(selectingPaymentMethod(timer))
      case CheckOutClosed                 => context.become(closed)
      case CheckoutCancelled              => context.become(cancelled)
      case PaymentStarted(payment)        => context.become(processingPayment(timer))
    }
  }

  def receiveCommand: Receive = {
    case StartCheckout =>
      val timer = schedule
      persist(CheckoutStarted)(event => updateState(event, Some(timer)))
  }

  def selectingDelivery(timer: Cancellable): Receive = {
    case SelectDeliveryMethod(method) =>
      persist(DeliveryMethodSelected(method))(event => updateState(event, Some(timer)))
    case CancelCheckout | Expire => persist(CheckoutCancelled)(event => updateState(event, Some(timer)))
  }

  def selectingPaymentMethod(timer: Cancellable): Receive = {
    case SelectPayment(paymentMethod) =>
      Helper.withCancel(timer) {
        val paymentTimer = schedule
        val paymentActor = context.actorOf(Payment.props(paymentMethod, sender(), self), Random.alphanumeric.take(256).mkString)
        val event = PaymentStarted(paymentActor)
        persist(event) { savedEvent =>
          sender() ! savedEvent
          updateState(savedEvent, Some(paymentTimer))
        }
      }
    case CancelCheckout | Expire => persist(CheckoutCancelled)(event => updateState(event, Some(timer)))
  }

  def processingPayment(timer: Cancellable): Receive = {
    case ReceivePayment =>
      timer.cancel()
      cartActor ! CartActor.CloseCheckout
      persist(CheckOutClosed)(event => updateState(event))
    case CancelCheckout | Expire => persist(CheckoutCancelled)(event => updateState(event))
  }

  def cancelled: Receive = {
    case e => log.debug("Cancelled", e)
  }

  def closed: Receive = {
    case e => log.debug("Closed", e)
  }

  override def receiveRecover: Receive = {
    case event: Event     => updateState(event)
    case _: SnapshotOffer => log.error("Unhandled.")
  }

  private def schedule: Cancellable = scheduler.scheduleOnce(delay = timerDuration, receiver = self, message = Expire)
}
