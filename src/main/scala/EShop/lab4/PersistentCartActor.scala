package EShop.lab4

import EShop.lab2.{Cart, Checkout}
import akka.actor.{Cancellable, Props}
import akka.event.Logging
import akka.persistence.{PersistentActor, SnapshotOffer}

import scala.concurrent.duration._

object PersistentCartActor {

  def props(persistenceId: String) = Props(new PersistentCartActor(persistenceId))
}

class PersistentCartActor(
  val persistenceId: String
) extends PersistentActor {

  import EShop.lab2.CartActor._

  private val log       = Logging(context.system, this)
  val cartTimerDuration = 5.seconds

  private def scheduleTimer: Cancellable =
    context.system.scheduler.scheduleOnce(cartTimerDuration, self, ExpireCart)(context.system.dispatcher)

  override def receiveCommand: Receive = empty

  private def updateState(event: Event, timer: Option[Cancellable] = None): Unit = {
    timer.foreach(_.cancel)
    event match {
      case CartExpired | CheckoutClosed | CartEmptied                             => context.become(empty)
      case CheckoutCancelled(cart)                                                => context.become(nonEmpty(cart, scheduleTimer))
      case ItemAdded(item, cart)                                                  => context.become(nonEmpty(cart.addItem(item), scheduleTimer))
      case ItemRemoved(item, cart) if cart.size == 1 && cart.items.contains(item) => context.become(empty)
      case ItemRemoved(item, cart)                                                => context.become(nonEmpty(cart.removeItem(item), scheduleTimer))
      case CheckoutStarted(_, cart)                                               => context.become(inCheckout(cart))
    }
  }

  def empty: Receive = {
    case AddItem(item) => persist(ItemAdded(item, Cart.empty))(event => updateState(event, None))
    case GetItems      => sender() ! Cart.empty
  }

  def nonEmpty(cart: Cart, timer: Cancellable): Receive = {
    case RemoveItem(item) if cart.size == 1 && cart.items.contains(item) =>
      persist(CartEmptied)(event => updateState(event, Some(timer)))
    case RemoveItem(item) if cart.contains(item) =>
      persist(ItemRemoved(item, cart))(event => updateState(event, Some(timer)))
    case AddItem(item) =>
      persist(ItemAdded(item, cart))(event => updateState(event, Some(timer)))
    case StartCheckout =>
      val persistentCheckout = context.actorOf(PersistentCheckout.props(self, "persistent-checkout-id"), "persistentCheckout")
      val event              = CheckoutStarted(persistentCheckout, cart)
      persist(event) { _ =>
        persistentCheckout ! Checkout.StartCheckout
        sender() ! event
        updateState(event, Some(timer))
      }
    case ExpireCart =>
      persist(CartExpired)(event => updateState(event, Some(timer)))
    case GetItems => sender() ! cart
  }

  def inCheckout(cart: Cart): Receive = {
    case CloseCheckout  => persist(CheckoutClosed)(updateState(_))
    case CancelCheckout => persist(CheckoutCancelled(cart))(updateState(_))
  }

  override def receiveRecover: Receive = {
    case event: Event     => updateState(event)
    case _: SnapshotOffer => log.error("Unhandled.")
  }
}
