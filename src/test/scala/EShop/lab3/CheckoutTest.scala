package EShop.lab3

import EShop.lab2.CartActor.CloseCheckout
import EShop.lab2.Checkout
import EShop.lab2.Checkout.PaymentStarted
import EShop.lab3.Payment.DoPayment
import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

class CheckoutTest
  extends TestKit(ActorSystem("CheckoutTest"))
  with FlatSpecLike
  with ImplicitSender
  with BeforeAndAfterAll
  with Matchers
  with ScalaFutures {

  override def afterAll: Unit =
    TestKit.shutdownActorSystem(system)

  it should "Send close confirmation to cart" in {
    val checkout = TestActorRef(new Checkout(self))
    checkout ! Checkout.StartCheckout
    checkout ! Checkout.SelectDeliveryMethod("dhl")
    checkout ! Checkout.SelectPayment("card")

    val paymentActorRef = expectMsgPF() {
      case PaymentStarted(payment) => payment
    }
    paymentActorRef ! DoPayment
    expectMsg(CloseCheckout)
  }

}
